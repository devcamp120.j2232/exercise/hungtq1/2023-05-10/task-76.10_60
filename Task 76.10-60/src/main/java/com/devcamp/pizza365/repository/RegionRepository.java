package com.devcamp.pizza365.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.CRegion;

public interface RegionRepository extends JpaRepository<CRegion, Long> {
	 List<CRegion> findByCountryId(Long countryId);
	 Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);
	 CRegion findByRegionCode(String regionCode);
	 Optional<CRegion> findByRegionCodeAndCountryCountryCode(String countryCode,String regionCode);
}
