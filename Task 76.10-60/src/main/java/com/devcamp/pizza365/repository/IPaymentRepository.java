package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Payment;

public interface IPaymentRepository extends JpaRepository<Payment, Integer> {
  @Query(value = "SELECT * FROM payments WHERE check_number LIKE :checkNumber%", nativeQuery = true)
  List<Payment> findByCheckNumberLike(@Param("checkNumber") String checkNumber);

  @Query(value = "SELECT * FROM payments WHERE payment_date LIKE :paymentDate%", nativeQuery = true)
  List<Payment> findByPaymentDateLike(@Param("paymentDate") String paymentDate, Pageable pageable);

  @Query(value = "SELECT * FROM payments WHERE ammount LIKE :ammount%", nativeQuery = true)
  List<Payment> findByAmmountLike(@Param("ammount") String ammount, Pageable pageable);

  @Query(value = "SELECT * FROM payments WHERE ammount = :ammount ORDER BY check_number ASC", nativeQuery = true)
  List<Payment> findByAmmountFromCheckNumberLike(@Param("ammount") String ammount, Pageable pageable);

  @Transactional
  @Modifying
  @Query(value = "UPDATE payments SET ammount = :ammount WHERE check_number  = :checkNumber", nativeQuery = true)
  int updateAmmount(@Param("checkNumber") String checkNumber, @Param("ammount") String ammount);
}
